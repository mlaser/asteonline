SPRINT BACKLOG - 4/04/2017 - 13/04/2017

- modello di dominio (Domain Model)
- modello dei casi d'uso (Use Case diagram)
- descrizione dettagliata dei casi d'uso
- modello delle classi (Class Diagram)
- glossario / altri artefatti