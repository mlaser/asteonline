﻿Glossario dei termini del modello di Dominio

Visitatore:
utilizzatore dell'applicazione in grado di avere accesso solo a poche funzioni,
cioè poter navigare le categorie degli articoli in vendita, effettuare la registrazione ed il login.

Utente:
utilizzatore dell'applicazione che ha effettuato il login (quindi dispone di credenziali di accesso,
create durante la registrazione). Dispone di più funzionalità rispetto ad un Visitatore, infatti può anche
fare Offerte per un articolo in vendita (quindi partecipare ad un'asta) ed effettuare domande ai proprietari
di un'asta in corso. Può, inoltre, mettere in vendita Articoli e rispondere alle domande fatte da altri utenti.
Può anche accedere al proprio Profilo per controllare lo stato delle aste a cui partecipa o degli Articoli che ha messo in vendita.

Admin:
Utente speciale registrato in automatico all'avvio del sistema. Dopo il login ha accesso a delle funzionalità esclusive quali aggiungere e rinominare Categorie (e sottocategorie),
eliminare Articoli in vendita, eliminare Offerte effettuate dagli utenti ed eliminare le Domande e risposte relative ad un'Asta.

Profilo:
E' il profilo da cui un Utente può vedere le proprie informazioni personali, le offerte che ha effettuato per gli articoli (con dettaglio 
sullo stato dell'asta), e lo stato delle aste per gli articoli che ha messo in vendita.

Domanda:
Entità che comprende una domanda posta da un'utente relativamente ad un articolo in vendita e la risposta relativa ad essa che verrà 
inserita dal proprietario dell'asta.

Asta:
Rappresenta il modo in cui viene venduto un oggetto, sono create dagli Utenti dell'applicazione. 
Hanno una data di scadenza, un prezzo base e un proprietario e dalla pagina di visualizzazione di un'asta è possibile vedere le Offerte effettuate
per quel determinato articolo, oltre che le domande poste dagli utenti.

Offerta:
Rappresenta il modo in cui un utente può aggiudicarsi un Articolo in un'Asta. Ogni Utente può effettuare delle Offerte al fine di acquistare un Articolo.

Articolo:
Entità che rappresenta l'articolo da vendere/acquistare.

Categoria:
Entità che raggruppa più Articoli e li suddivide secondo aspetti comuni. Una Categoria può avere a sua volta delle sottocategorie con caratteristiche simili ma più specifiche.

