package asteOnline;

import static org.junit.Assert.*;

import org.junit.Test;

import asteOnline.client.Utente;

public class testRegistrazione {
	JUnit test = new JUnit();
	
	@Test
	public void testAdmin() {
		test.pulisciDB();
		String[] testLoginAdmin = new String[2];
		test.adminReg();
		testLoginAdmin[0]="admin";
		testLoginAdmin[1]="admin";
		String result = test.login(testLoginAdmin);
		assertEquals(testLoginAdmin[0], result); //Test login admin
		
	}
	
	@Test
	public void testRegLogUtente(){
		test.pulisciDB();
		String[] nuovoUtente = new String[11];
		nuovoUtente[0]="ale.serra";
		nuovoUtente[1]="1234";
		nuovoUtente[2]="Alessandro";
		nuovoUtente[3]="Serra";
		nuovoUtente[4]="3334619645";
		nuovoUtente[5]="serra.alessandro@hotmail.com";
		nuovoUtente[6]="SRRLSN944ECCETERA";
		nuovoUtente[7]="Via Ramenghi 10";
		nuovoUtente[8]="Maschio";
		nuovoUtente[9]="1994-11-01";
		nuovoUtente[10]="Bologna";
		String result2 = test.register(nuovoUtente);
		assertEquals(nuovoUtente[0],result2); //Test registrazione utente
		String result4 = test.login(nuovoUtente);
		assertEquals(nuovoUtente[0], result4); //Test login utente
		String result3 = test.register(nuovoUtente);
		assertEquals("Utente gi� registrato!", result3);//test utent egi� registrato
		
	}
	
	@Test
	public void infoUserTest(){
		test.pulisciDB();
		String[] nuovoUtente = new String[11];
		nuovoUtente[0]="ale.serra";
		nuovoUtente[1]="1234";
		nuovoUtente[2]="Alessandro";
		nuovoUtente[3]="Serra";
		nuovoUtente[4]="3334619645";
		nuovoUtente[5]="serra.alessandro@hotmail.com";
		nuovoUtente[6]="SRRLSN944ECCETERA";
		nuovoUtente[7]="Via Ramenghi 10";
		nuovoUtente[8]="Maschio";
		nuovoUtente[9]="1994-11-01";
		nuovoUtente[10]="Bologna";
		test.register(nuovoUtente);
		Utente result = test.infoUser(nuovoUtente[0]);
		//Test per tutti i campi di infoUser
		assertEquals(nuovoUtente[0], result.getUsername());
		assertEquals(nuovoUtente[1], result.getPassword());
		assertEquals(nuovoUtente[2], result.getNome());
		assertEquals(nuovoUtente[3], result.getCognome());
		assertEquals(nuovoUtente[4], result.getNumeroTelefono());
		assertEquals(nuovoUtente[5], result.getEmail());
		assertEquals(nuovoUtente[6], result.getCodiceFiscale());
		assertEquals(nuovoUtente[7], result.getDomicilio());
		assertEquals(nuovoUtente[8], result.getSesso());
		assertEquals(nuovoUtente[9], result.getDataNascita());
		assertEquals(nuovoUtente[10], result.getLuogoNascita());
	}

}
