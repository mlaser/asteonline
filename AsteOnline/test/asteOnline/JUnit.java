package asteOnline;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import asteOnline.client.Asta;
import asteOnline.client.Domanda;
import asteOnline.client.Offerta;
import asteOnline.client.Utente;

public class JUnit {

	public DB getDB() {
		
			DB db = DBMaker.newFileDB(new File("dbTest")).closeOnJvmShutdown().make();
			
			return db;
	}
	
	public void pulisciDB(){
		File db = new File("dbTest");
		db.delete();
	}
	
	//Test registrazione e login
	public int checkRegistrazione(String input){
		DB users = getDB();
		Map <String, Utente> myMap = users.getTreeMap("utenti");
		Utente value = myMap.get(input);
		if (value!=null){
			String cntrl = value.getUsername();
			if(input.equals(cntrl)){
				users.commit();
				return 1;
			}
		}
		users.commit();
		return 0;
	}
	
	public void adminReg() throws IllegalArgumentException {
		Utente admin = new Utente();
		if(checkRegistrazione(admin.getUsername())==0){
			DB users = getDB();
			Map <String, Utente> myMap = users.getTreeMap("utenti");
			myMap.put(admin.getUsername(), admin);
			users.commit();
		}
	}
	
	public String login(String input[]) throws IllegalArgumentException {
		DB users = getDB();
		Map <String, Utente> myMap = users.getTreeMap("utenti");
		Utente value = myMap.get(input[0]);
		if(value!=null){
			if(value.getPassword().equals(input[1])) {
				return input[0];
			} else return "Password errata";
		} else return "Utente non esistente";
	}
	
	public String register(String input[]) throws IllegalArgumentException {

		if(checkRegistrazione(input[0])==0){
			for(int i=0; i<8; i++){
				if(input[i]==null) return "Campi mancanti, compilare tutti i campi obbligatori!";
			}
			DB users = getDB();
			Utente newUser=new Utente(input[0],input[1],input[2],input[3],input[4],
				input[5],input[6],input[7]);
			if(input[8]!=null) newUser.setSesso(input[8]); //Controllo e inserimento per campo opzionale Sesso
			if(input[9]!=null) newUser.setDataNascita(input[9]); //Controllo e inserimento per campo opzionale Data di Nascita
			if(input[10]!=null) newUser.setLuogoNascita(input[10]); //Controllo e inserimento per campo opzionale Luogo di Nascita
			Map <String, Utente> myMap = users.getTreeMap("utenti");
			myMap.put(input[0], newUser);
			Utente value = myMap.get(input[0]);
			users.commit();
			return value.getUsername();
		}else return "Utente gi� registrato!";
	}
	
	public Utente infoUser(String input) throws IllegalArgumentException{
		DB users = getDB();
		Map <String, Utente> myMap = users.getTreeMap("utenti");
		Utente value = myMap.get(input);
		return value;
	}
	//Fine test registrazione e login
	
	//Test aste
	public String inserisciDomanda(Domanda input){
		DB db = getDB();
		Map<Integer, Domanda> map = db.getTreeMap("domanda");
		int max=0;		
		Set<Integer> keys = map.keySet();
		for(int key : keys) {			
			if(key>max){
				max=key;				
			}
		}
		input.setCodiceDomanda(max+1);
		map.put(max+1, input);
		Domanda inserita = map.get(max+1);
		db.commit();
		String testo = inserita.getDomanda();
		String user = inserita.getUtente();
		int codice = inserita.getCodiceAsta();
		return testo+" "+user+" "+codice;
	}
	
	public String inserisciRisposta(Domanda input){
		DB db = getDB();
		Map<Integer, Domanda> map = db.getTreeMap("domanda");
		map.put(input.getCodiceDomanda(), input);
		String risposta = map.get(input.getCodiceDomanda()).getRisposta();
		db.commit();
		return risposta;
	}
	
	public int saveAsta(Asta oggAsta) {
		DB db = getDB();
		Map<Integer, Asta> map = db.getTreeMap("asta");
		int max=0;
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(key>max){
				max=key;
			}
		}
		
		oggAsta.setCodiceAsta(max+1);
		map.put(max+1, oggAsta);
		db.commit(); 
		return 1;
	}
	
	public int saveOfferta(Offerta offer) {
		DB db = getDB();
		Map<Integer, Offerta> map = db.getTreeMap("offerta");
		int max=0;
		double maxOfferta=0;
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(key>max){
				max=key;
			}
			if(map.get(key).getCodiceAsta()==offer.getCodiceAsta()){
				if(map.get(key).getImporto()>maxOfferta){
					maxOfferta=map.get(key).getImporto();	
				}
			}
		}
		if(offer.getImporto()>maxOfferta){
		offer.setCodiceOfferta(max+1);
		map.put(max+1, offer);
		db.commit();
		return 1;
		}else{
			return 0;
		}
	}
	
	private int statusAsta(String dataAsta){
		Date today = new Date();
		int anno=Integer.parseInt(dataAsta.substring(0,4));
		int mese=Integer.parseInt(dataAsta.substring(5,7));
		int giorno=Integer.parseInt(dataAsta.substring(8,10));
		int giornoOggi = today.getDate();
		int meseOggi=today.getMonth()+1;
		int annoOggi=today.getYear()+1900;
		int stato=0;
	    if(anno>annoOggi||(anno==annoOggi&&mese>meseOggi)||(anno==annoOggi&&mese==meseOggi&&giorno>giornoOggi)) 
	    	stato=1;
	    return stato;
	}
	
	public String statoOfferta(int input){
		DB db = getDB();
		Map<Integer, Offerta> map = db.getTreeMap("offerta");
		double impOff = map.get(input).getImporto();
		int codAsta = map.get(input).getCodiceAsta();
		int status=0;
		Set<Integer> keys = map.keySet();
		for(int key : keys){
			if(map.get(key).getCodiceAsta()==codAsta){
				if(map.get(key).getImporto()>impOff) status=1;
			}
		}
		DB db1 = getDB();
		Map<Integer, Asta> map1 = db1.getTreeMap("asta");
		int cntrl= statusAsta(map1.get(codAsta).getDataScadenza());
	    if(status==0)
	    	if(cntrl==1) return "Asta in corso e offerta migliore";
	    if(status==1)
	    	if(cntrl==1) return "Asta in corso e offerta superata";
	    if(status==1)
	    	if(cntrl==0) return "Asta conclusa e offerta superata";
	    return "Asta conclusa e offerta migliore";
	}
	
	public String descrAsta(int input){
		DB db = getDB();
		Map<Integer, Asta> map = db.getTreeMap("asta");
		Set<Integer> keys = map.keySet();
		for(int key : keys){
			if(map.get(key).getCodiceAsta()==input) return map.get(key).getDescrizione();
		}
		return "Errore";
	}

	//Fine test Aste
}
