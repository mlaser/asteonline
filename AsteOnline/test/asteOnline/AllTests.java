package asteOnline;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	testRegistrazione.class,
	testAste.class
})


public class AllTests {

}
