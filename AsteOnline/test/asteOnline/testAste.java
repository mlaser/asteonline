package asteOnline;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import asteOnline.client.Asta;
import asteOnline.client.Domanda;
import asteOnline.client.Offerta;

public class testAste {
	JUnit test = new JUnit();
	@Test
	public void testDomandaRisposta() {
		Domanda testDomanda = new Domanda(1, "ale.serra", "Prova");
		String testString = testDomanda.getDomanda()+" "+testDomanda.getUtente()+" "+testDomanda.getCodiceAsta();
		String result = test.inserisciDomanda(testDomanda);
		assertEquals(testString, result);//test inserimento domanda
		testDomanda.setRisposta("testRisposta");
		String result2 = test.inserisciRisposta(testDomanda);
		assertEquals(testDomanda.getRisposta(), result2); //test inserimento risposta
	}
	
	@Test
	public void testAsta(){
		ArrayList<Integer> catTest=new ArrayList<Integer>();
		catTest.add(1);
		Asta testAsta = new Asta("ale.serra", 120, "2017-05-10","testAsta",catTest);
		assertEquals(1, test.saveAsta(testAsta)); //Test inserimento asta
	}
	
	@Test
	public void testOfferta(){
		test.pulisciDB();
		ArrayList<Integer> catTest=new ArrayList<Integer>();
		catTest.add(1);
		Asta testAsta = new Asta("ale.serra", 120, "2018-10-10","testAsta",catTest);
		test.saveAsta(testAsta);
		Offerta testOfferta = new Offerta("ale.serra", 125, 1,1);
		Offerta testOfferta2 = new Offerta("vins.morix", 130, 1,2);
		assertEquals(1, test.saveOfferta(testOfferta)); //Test inserimento offerta
		test.saveOfferta(testOfferta2);
		String result = test.statoOfferta(1);
		String result2 = test.statoOfferta(2);
		assertEquals("Asta in corso e offerta superata", result); //Test stato offerta superata
		assertEquals("Asta in corso e offerta migliore", result2); //Test stato offerta migliore
	}
	
	@Test
	public void testErroreAsta(){
		test.pulisciDB();
		assertEquals("Errore", test.descrAsta(99));//Test asta non esistente
	}
}
