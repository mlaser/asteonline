package asteOnline.client;

import java.io.Serializable;
import java.util.ArrayList;

public class Asta implements Serializable {

	private static final long serialVersionUID = 23L;
	private String proprietario;
	private double prezzoBase;
	private String dataScadenza;
	private String descrizione;
	private ArrayList<Integer> categoria;
	private int codiceAsta=0;
	// Costruttore
	public Asta (String proprietario, double prezzoBase, String dataScadenza,String descrizione,ArrayList<Integer> categoria){
		
		this.proprietario=proprietario;
		this.descrizione=descrizione;
		this.prezzoBase=prezzoBase;
		this.dataScadenza=dataScadenza;
		this.categoria=categoria;
	}
	public Asta(){}
	
	// SETTER
	
	public void setDataScadenza(String dataScadenza){
		this.dataScadenza=dataScadenza;
	}
	
	// GETTER
	public double getPrezzo(){
		return prezzoBase;
		
	}
	public String getProprietario(){
		return proprietario;
	}
	
	public String getDescrizione(){
		return  descrizione;
	}
	
	public String getDataScadenza(){
		return dataScadenza;
	}
	public ArrayList<Integer> getCategoria(){
		
		return categoria;
	}
	
	public void setCodiceAsta(int codice){
		codiceAsta=codice;
	}
	// Metodi generici
	
	public int getCodiceAsta(){
		
	return codiceAsta;	
	}
}
