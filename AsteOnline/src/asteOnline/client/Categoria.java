package asteOnline.client;

import java.io.Serializable;

public class Categoria implements Serializable{
	private static final long serialVersionUID = 2L;
	private String nome;
	private int idPadre;
	public Categoria(){
		nome="cat";
		idPadre=0;
		
	}
	public Categoria ( String nome, int idPadre){
	
		this.nome=nome;
		this.idPadre=idPadre;
	}
	
	
	
	
	public int getPadre(){
		return idPadre;
	}
	
	public String getNome(){
		return nome;
	}

}
