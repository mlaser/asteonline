package asteOnline.client;

import java.io.Serializable;

public class Domanda implements Serializable{
	
	private static final long serialVersionUID = -6797200655479877244L;
	private int codiceDomanda;
	private int codiceAsta;
	private String utente;
	private String domanda;
	private String risposta;
	

	public Domanda (int codiceAsta, String utente, String domanda){
		this.codiceAsta=codiceAsta;
		this.utente=utente;
		this.domanda=domanda;
		
	}
	public Domanda(){}
	// SETTER
	
	public void setRisposta(String risposta){
		this.risposta=risposta;
	}
	public void setCodiceDomanda(int codiceDomanda) {
		this.codiceDomanda = codiceDomanda;
	}
	
	// GETTER
	public int getCodiceAsta(){
		return codiceAsta;
	}
	
	public String getDomanda(){
		return domanda;
	}
	
	public String getRisposta(){
		return risposta;
	}
	
	public String getUtente(){
		return utente;
	}
	public int getCodiceDomanda() {
		return codiceDomanda;
	}
	
	
}
