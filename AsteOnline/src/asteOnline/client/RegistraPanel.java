package asteOnline.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class RegistraPanel {
	private LoginRegisterServiceAsync loginRegSvc = GWT.create(LoginRegisterService.class);
	
	final Button registraButton = new Button("Conferma");
	final TextBox userField = new TextBox();
	final TextBox passwordField = new PasswordTextBox();
	final TextBox nomeField = new TextBox();
	final TextBox cognomeField = new TextBox();
	final TextBox telefonoField = new TextBox();
	final TextBox emailField = new TextBox();
	final TextBox cfField = new TextBox();
	final TextBox domicilioField = new TextBox();
	final ListBox sessoBox = new ListBox();
	final TextBox dataNField = new TextBox();
	final TextBox luogoNField = new TextBox();
	
    public RegistraPanel(){
    	
    	
    }
    public void creaPanel(){
    	RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
    	Label lbReg = new Label("Registrati");
    	HTML lbSesso = new HTML("<b>Sesso:</b>  <b style='margin-left:31pt'>Data di nascita: </b>");
    	lbReg.getElement().setAttribute("style", "font-size:21pt; margin-bottom:5pt");
    	userField.getElement().setAttribute("placeHolder", "Username");
    	userField.getElement().setAttribute("style", "margin-right:5pt");
    	passwordField.getElement().setAttribute("placeHolder", "Password");
    	passwordField.getElement().setAttribute("style", "margin-right:10pt");
    	nomeField.getElement().setAttribute("placeHolder", "Nome");
    	nomeField.getElement().setAttribute("style", "margin-right:5pt");
    	cognomeField.getElement().setAttribute("placeHolder", "Cognome");
    	cognomeField.getElement().setAttribute("style", "margin-right:5pt");
    	telefonoField.getElement().setAttribute("placeHolder", "Numero di Telefono");
    	telefonoField.getElement().setAttribute("style", "margin-right:5pt");
    	emailField.getElement().setAttribute("placeHolder", "email");
    	emailField.getElement().setAttribute("style", "margin-right:5pt");
    	cfField.getElement().setAttribute("placeHolder", "Codice Fiscale");
    	cfField.getElement().setAttribute("style", "margin-right:5pt");
    	domicilioField.getElement().setAttribute("placeHolder", "Domicilio");
    	domicilioField.getElement().setAttribute("style", "margin-right:5pt");
    	sessoBox.addItem("");
    	sessoBox.addItem("Maschio");
    	sessoBox.addItem("Femmina");
    	sessoBox.setVisibleItemCount(1);
    	sessoBox.getElement().setAttribute("style", "margin-right:5pt");
    	dataNField.getElement().setAttribute("placeHolder", "Data di nascita");
    	dataNField.getElement().setAttribute("style", "margin-right:5pt");
    	dataNField.getElement().setAttribute("type", "date");
    	luogoNField.getElement().setAttribute("placeHolder", "Luogo di nascita");
    	luogoNField.getElement().setAttribute("style", "margin-right:5pt");
    	
    	RootPanel.get("livello1").add(lbReg);
    	RootPanel.get("livello1").add(userField);
		RootPanel.get("livello1").add(passwordField);
		RootPanel.get("livello1").add(nomeField);
		RootPanel.get("livello1").add(cognomeField);
		RootPanel.get("livello2").add(telefonoField);
		RootPanel.get("livello2").add(emailField);
		RootPanel.get("livello2").add(cfField);
		RootPanel.get("livello2").add(domicilioField);
		RootPanel.get("livello3").add(lbSesso);
		RootPanel.get("livello3").add(sessoBox);
		RootPanel.get("livello3").add(dataNField);
		RootPanel.get("livello3").add(luogoNField);
		RootPanel.get("livello3").add(registraButton);
    
    
		//INIZIO REGISTRA
  		//Controllo input numero di telefono
  		telefonoField.addKeyUpHandler(new KeyUpHandler() {
  		    @Override
  		    public void onKeyUp(KeyUpEvent event) {
  		        String input = telefonoField.getText();
  		        if (!input.matches("[0-9]*")) {
  		        	Window.alert("Inserire solo valori numerici!");
  		        	telefonoField.setText(null);
  		        	return;
  		        }
  		    }
  		});
  		//Fine controllo input numero di telefono
  		//Inizio controllo input username
  		userField.addKeyUpHandler(new KeyUpHandler() {
  		    @Override
  		    public void onKeyUp(KeyUpEvent event) {
  		        String input = userField.getText();
  		        if (!input.matches("[A-Za-z0-9._]*")) {
  		        	Window.alert("Inserire solo valori alfanumerici!");
  		        	userField.setText(null);
  		        	return;
  		        }
  		    }
  		});
  		//Fine controllo input username
  		//Handler per pulsante di registrazione
  		class MyHandler implements ClickHandler, KeyUpHandler {
  			private void sendNameToServer() {
  				String[] userToServer = new String[11];
  				userToServer[0] = userField.getText();
  				userToServer[1] = passwordField.getText();
  				userToServer[2] = nomeField.getText();
  				userToServer[3] = cognomeField.getText();
  				userToServer[4] = telefonoField.getText(); 
  				userToServer[5] = emailField.getText();
  				userToServer[6] = cfField.getText().toUpperCase();
  				userToServer[7] = domicilioField.getText();
  				userToServer[8] = sessoBox.getSelectedItemText();
  				userToServer[9] = dataNField.getText();
  				userToServer[10] = luogoNField.getText();
  				//Window.alert(userToServer[9]);
  				for(int i=0; i<8; i++){
  					if(userToServer[i].isEmpty()) {
  						Window.alert("Campi obbligatori non compilati, ricontrollare!");
  						return;
  					}
  				}
  				
  				registraButton.setEnabled(false);
  				loginRegSvc.register(userToServer, new AsyncCallback<String>() {
  					@Override
  					public void onFailure(Throwable caught) {
  						Window.alert(caught.toString());
  						registraButton.setEnabled(true);
  					}
  					@Override
  					public void onSuccess(String result) {
  						Window.alert(result);
  						registraButton.setEnabled(true);
  						RootPanel.get("livello1").clear();
  				    	RootPanel.get("livello2").clear();
  				    	RootPanel.get("livello3").clear();
  					}
  				});
  			}
  					
  			public void onClick(ClickEvent event) {
  				sendNameToServer();
  			}
  			public void onKeyUp(KeyUpEvent event) {
  				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
  					sendNameToServer();
  				}
  			}
  		}

  		// Add a handler to send the name to the server
  		MyHandler handler = new MyHandler();
  		registraButton.addClickHandler(handler);
  		userField.addKeyUpHandler(handler);
  		//FINE REGISTRA
    
    }
}