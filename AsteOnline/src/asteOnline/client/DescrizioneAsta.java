package asteOnline.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class DescrizioneAsta implements Serializable {

	private static final long serialVersionUID = 1L;
	String descrizione;
	String scadenza;
	String proprietario;
	double prezzo;
	int codiceAsta;
	private AstaServiceAsync astaSvc = GWT.create(AstaService.class);
	TextBox offertaText=new TextBox();
	
	public DescrizioneAsta(){}

	private void eliminaDomande(int codice){
    	astaSvc.deleteDomanda(codice, new AsyncCallback<Void>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}

			@Override
			public void onSuccess(Void result) {
				creaPanel("admin");				
			}
    		
    	});
    }
	
	private void eliminaOfferta(final int codiceOfferta){
		astaSvc.elimnaOfferta(codiceOfferta, new AsyncCallback<Void>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}

			@Override
			public void onSuccess(Void result) {
				creaPanel("admin");
			}
			
		});
	}

	public DescrizioneAsta(String descrizione,String scadenza,String proprietario,double prezzo,int codiceAsta){
	
	this.descrizione=descrizione;
	this.scadenza=scadenza;
	this.proprietario=proprietario;
	this.prezzo=prezzo;
	this.codiceAsta=codiceAsta;
	

	}


	public void creaPanel(String utente){
	 RootPanel.get("livello1").clear();
	 RootPanel.get("livello2").clear();
	 RootPanel.get("livello3").clear();
	 final String user=utente;
	 Label descr=new Label(descrizione);
	 descr.getElement().getStyle().setFontSize(50, Unit.PX);
	 RootPanel.get("livello1").add(descr);
	 
	 Label scad=new Label("Scandenza: "+scadenza);
	 scad.getElement().getStyle().setFontSize(20, Unit.PX);
	 RootPanel.get("livello2").add(scad);
	
	 Label propr=new Label("Proprietario: "+ proprietario);
	 propr.getElement().getStyle().setFontSize(20, Unit.PX);
	 RootPanel.get("livello2").add(propr);
	 Button nuovaOfferta=new Button("Fai una Offerta");

	 if(!utente.equals(null)){
		 if(!utente.equals("admin")){
			 if(!proprietario.equals(utente)){
				 Label off=new Label("Offri subito!");
				 off.getElement().getStyle().setFontSize(20, Unit.PX);
			 
			 
				 RootPanel.get("livello3").add(off);
				 RootPanel.get("livello3").add(offertaText);
				 RootPanel.get("livello3").add(nuovaOfferta);
			 }
		 }
	 }
	 
	 offertaText.addKeyUpHandler(new KeyUpHandler() {
		    @Override
		    public void onKeyUp(KeyUpEvent event) {
		       String input=offertaText.getText();
		        if (!input.matches("[0-9.]*")) {
		        	Window.alert("Inserire solo valori numerici!");
		        	offertaText.setText(null);
		        	return;
		        }
		    }
		
			
		});
	 
	 nuovaOfferta.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				double prezzoOfferto=Double.parseDouble(offertaText.getText());
				Offerta offer=new Offerta(user,prezzoOfferto,codiceAsta,0);
				if(prezzoOfferto>prezzo){
					astaSvc.saveOfferta(offer, new AsyncCallback<Integer>() {
					
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Cannot load symbols: "
									+ caught.getMessage());
						}
					
						@Override
						public void onSuccess(Integer result) {
							if(result==1){
								Window.alert("Offerta inserita");
								offertaText.setText("");
								creaPanel(user);
							}else{
								Window.alert("Errore! Offerta troppo Bassa!");
							}
						}
					});
				}else{
					Window.alert("Errore! Offerta troppo Bassa!");
				}
			}
	 });
	 
	 astaSvc.loadOfferta(codiceAsta,new AsyncCallback<ArrayList<Offerta>>() {
		 
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore!");
			}
			
			@Override
			public void onSuccess(ArrayList<Offerta> result) {
				if(result.size()==0){
					 Label prezz=new Label("Ancora nessuna offerta! prezzo base: "+prezzo+" €");
					 prezz.getElement().getStyle().setFontSize(30, Unit.PX);
					 RootPanel.get("livello3").add(prezz);
				}else{
					FlexTable t = new FlexTable();
					t.setTitle("OFFERTE");
				    t.setText(0, 0, "UTENTE");
				    t.setText(0, 1, "IMPORTO OFFERTO(€)");
				    t.setWidth("300px");
				    t.setCellSpacing(50);
				    Collections.reverse(result);
					for(int i=0;i<result.size();i++){
						
					    t.setText(i+1, 0, result.get(i).getUtente());
					    t.setText(i+1, 1,""+ result.get(i).getImporto());
					    if(user.equals("admin")){
					    	Button elimina=new Button("elimina");
					    	t.setWidget(i+1,4,elimina);
					    	final int codiceOff=result.get(i).getCodiceOfferta();
					    	elimina.addClickHandler(new ClickHandler() {
					    		@Override
					    		public void onClick(ClickEvent event) {
					    			//Elimina offerta
					    			eliminaOfferta(codiceOff);
					    		}
					    	});
					    }
					}
					RootPanel.get("livello3").add(t);
				}
			}
		});
	 
	 //Domande articolo
	 
	 
	 
	//Carica domande

     astaSvc.loadDomande(codiceAsta, new AsyncCallback<ArrayList<Domanda>>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}

			@Override
			public void onSuccess(ArrayList<Domanda> result) {
				// TODO Auto-generated method stub
				for(int i=0; i<result.size();i++){
					final Domanda elemento = result.get(i);
					HTML elementoDomanda = new HTML("<br><b>Domanda: </b>"+elemento.getDomanda()+""
							+ "<br><b>Pubblicata da: </b>"+elemento.getUtente()+""
									+ "<br><b>Risposta: </b>"+elemento.getRisposta());
					RootPanel.get("livello3").add(elementoDomanda);
					Button rispondiButton = new Button("Rispondi");
					if (proprietario.equals(user)) 
						if((elemento.getRisposta().equals(null))) RootPanel.get("livello3").add(rispondiButton);
					rispondiButton.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent event) {
							//TODO Domanda
							RispostaPanel answerPanel= new RispostaPanel();
							answerPanel.creaPanel(elemento, user);
						}
					});
					if(user.equals("admin")) {
						Button eliminaButton = new Button("Elimina");
						eliminaButton.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								//TODO Domanda
								Window.alert("Domanda eliminata, codice domanda: "+elemento.getCodiceDomanda());
								eliminaDomande(elemento.getCodiceDomanda());
							}
						});
						RootPanel.get("livello3").add(eliminaButton);
					}
				}
			}
		});
     //Fine carica domande
     
   //Inserimento domanda
   	 Button domandeB=new Button("Fai una Domanda");
   	 domandeB.addClickHandler(new ClickHandler() {
   			@Override
   			public void onClick(ClickEvent event) {
   				DomandeRispostePanel drPanel = new DomandeRispostePanel();
   				drPanel.creaPanel(user, codiceAsta);
   			}
   		});
   	 if(!user.equals(null))
   		 if(!user.equals("admin"))
   			 if(!user.equals(proprietario))
   			 RootPanel.get("livello3").add(domandeB);
	}
}
