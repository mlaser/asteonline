package asteOnline.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

public class LoginPanel {
	private LoginRegisterServiceAsync loginRegSvc = GWT.create(LoginRegisterService.class);
	
	final Button loginButton = new Button("Login");
	final TextBox userLoginField = new TextBox();
	final TextBox passwordLoginField = new PasswordTextBox();
	
    public LoginPanel(){
    	
    	
    }
    public void creaPanel(){
    	RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
    	Label lbLog = new Label("Login");
    	lbLog.getElement().setAttribute("style", "font-size:21pt; margin-bottom:5pt");
    	userLoginField.getElement().setAttribute("placeHolder", "Username");
    	userLoginField.getElement().setAttribute("style", "margin-right:5pt");
		passwordLoginField.getElement().setAttribute("placeHolder", "Password");
		passwordLoginField.getElement().setAttribute("style", "margin-right:5pt");
    	
		RootPanel.get("livello1").add(lbLog);
    	RootPanel.get("livello1").add(userLoginField);
		RootPanel.get("livello1").add(passwordLoginField);
		RootPanel.get("livello1").add(loginButton);
		
		//INIZIO LOGIN
		userLoginField.addKeyUpHandler(new KeyUpHandler() {
  		    @Override
  		    public void onKeyUp(KeyUpEvent event) {
  		        String input = userLoginField.getText();
  		        if (!input.matches("[A-Za-z0-9._]*")) {
  		        	Window.alert("Inserire solo valori alfanumerici!");
  		        	userLoginField.setText(null);
  		        	return;
  		        }
  		    }
  		});
		// Create a handler for the registraButton and userField
		class MyHandlerLogin implements ClickHandler, KeyUpHandler {
			private void sendLoginToServer() {
				String[] loginToServer = new String[2];
				loginToServer[0] = userLoginField.getText();
				loginToServer[1] = passwordLoginField.getText();
						
				loginButton.setEnabled(false);
				loginRegSvc.login(loginToServer, new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					Window.alert(caught.toString());
					loginButton.setEnabled(true);
				}

				public void onSuccess(String result) {
					if(result.equals(userLoginField.getText())){
						Window.alert("Login effettuato con successo! Nome utente: "+result);
					} else Window.alert(result);
					loginButton.setEnabled(true);
					if(result.equals(userLoginField.getText())){
						AsteOnline rimuoviLogin = new AsteOnline();
						rimuoviLogin.effettuaLogin(result);
					}
				}
			});
		}
		public void onClick(ClickEvent event) {
			if(userLoginField.getText().equals("")) Window.alert("Inserire nome utente!");
			else sendLoginToServer();
		}
		public void onKeyUp(KeyUpEvent event) {
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				if(userLoginField.getText().equals("")) Window.alert("Inserire nome utente!");
				else sendLoginToServer();
			}
		}

	}
	MyHandlerLogin handlerLogin = new MyHandlerLogin();
	loginButton.addClickHandler(handlerLogin);
	userLoginField.addKeyUpHandler(handlerLogin);
	//FINE LOGIN
    
    
    }
}