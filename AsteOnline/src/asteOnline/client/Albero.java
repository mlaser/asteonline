package asteOnline.client;

import java.io.Serializable;

public class Albero implements Serializable {
	private static final long serialVersionUID = 34L;
	int id;
	Categoria cat;
	
	public Albero(){
  
	}
	
	public Albero(int id,Categoria cat){
		this.id=id;
		this.cat=cat;
	}
	
	public int getId(){
		return id;
	}
	
	public Categoria getCategoria(){
		return cat;
	}
}
