package asteOnline.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;

public class AstaPanel {
	Button nuovaAsta = new Button("Conferma");	
	ListBox listaCategorie=new ListBox();
	TextArea descrizione =new TextArea();
	TextBox prezzoBase =new TextBox();
	final TextBox dataScadenza = new TextBox();
	
	Label lb=new Label("DESCRIZIONE ARTICOLO:");
	Label lb2=new Label("PREZZO BASE:");
	Label lb3=new Label("SELEZIONA CATEGORIA:");
	Label lb4=new Label("SELEZIONA DATA SCADENZA:");
	private AstaServiceAsync astaSvc = GWT.create(AstaService.class);
	private CategoriaServiceAsync categoriaSvc = GWT.create(CategoriaService.class);
	String loggedUser;
	
	public AstaPanel(ListBox listaCat){
    	listaCategorie=listaCat;
    	
    	nuovaAsta.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//Controllo che tutti i campi siano stati compilati
				if(descrizione.getText().equals("")||dataScadenza.getText().equals("")||prezzoBase.getText().equals("")){
					Window.alert("Compila Tutti i Campi"+listaCategorie.getSelectedIndex()+1);}
				else{
					
				generaListaCat(listaCategorie.getSelectedIndex(),loggedUser);
				}
			}
		});
    	
    	prezzoBase.addKeyUpHandler(new KeyUpHandler() { //Handler per controllo input numerico
  		    @Override
  		    public void onKeyUp(KeyUpEvent event) {
  		        String input = prezzoBase.getText();
  		        if (!input.matches("[0-9]*")) {
  		        	Window.alert("Inserire solo valori numerici!");
  		        	prezzoBase.setText(null);
  		        	return;
  		        }
  		    }
  		});
    }
    
    public void creaPanel(String user){
    	dataScadenza.getElement().setAttribute("type", "date");
        descrizione.setCharacterWidth(80);
        descrizione.setVisibleLines(5);
    	RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
    	RootPanel.get("livello1").add(lb);
    	RootPanel.get("livello1").add(descrizione);
    	RootPanel.get("livello1").add(lb2);
    	RootPanel.get("livello1").add(prezzoBase);
    	RootPanel.get("livello1").add(lb3);
    	RootPanel.get("livello1").add(listaCategorie);
    	RootPanel.get("livello1").add(lb4);
    	RootPanel.get("livello1").add(dataScadenza);
    	RootPanel.get("livello3").add(nuovaAsta);
    	loggedUser=user;
    }
    public void generaListaCat(final int leaf, String utente){
    	
    	final ArrayList<Integer> catArt =new ArrayList<Integer>();
    	final String user1=utente;
    	
    	categoriaSvc.loadCategoria(new AsyncCallback<ArrayList<Albero>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore!");
			}

			@Override
			public void onSuccess(ArrayList<Albero> result) {
		
				int padrePartenza=result.get(leaf).getCategoria().getPadre();
				catArt.add(leaf+1);
				catArt.add(0);
				int check=1;
				for(int i=0;i<result.size();i++){
					if(check==1){
						check--;
					
						for(int j=0;j<result.size();j++){
							if(result.get(j).getId()==padrePartenza){
								catArt.add(padrePartenza);
								padrePartenza=result.get(j).getCategoria().getPadre();
								check++;
							}
						}
					}
				}				
			creaAsta(catArt, user1);
			}
		 });    
   }
   //Inizio metodo per creazione Asta 
   public void creaAsta(ArrayList<Integer> cat, String utente){
	   final String user2=utente;
	   int prezzo=Integer.parseInt(prezzoBase.getText());
	   Asta oggAsta=new Asta(utente,prezzo , dataScadenza.getText(),descrizione.getText() , cat);
		
	   astaSvc.saveAsta(oggAsta, new AsyncCallback<Integer>() {
		   @Override
		   public void onFailure(Throwable caught) {
			   Window.alert("Cannot load symbols: "
					   + caught.getMessage());
			}

			@Override
			public void onSuccess(Integer result) {
				Window.alert("Asta creata con successo!");
				AsteOnline mainPanel = new AsteOnline();
				mainPanel.effettuaLogin(user2);
			}
		});
   	}
   //Fine metodo per creazione asta
}
