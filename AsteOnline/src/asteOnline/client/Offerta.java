package asteOnline.client;

import java.io.Serializable;

public class Offerta implements Serializable{
	private static final long serialVersionUID = 1L;
	private String utente;
	private double importoOfferto;
	private int codiceAsta;
	private int codiceOfferta;
	
	// Costruttore
	public Offerta (String utente, double importo, int codiceAsta,int codiceOfferta){
		this.utente=utente;
		this.importoOfferto=importo;
		this.codiceAsta=codiceAsta;
		this.codiceOfferta=codiceOfferta;
	}
	
	public Offerta(){}
	
	// GETTER
	
	public String getUtente(){
		return utente;
	}
	
	public double getImporto(){
		return importoOfferto;
	}
	public int getCodiceOfferta(){
		return codiceOfferta;
	}
	public void setCodiceOfferta(int cod){
		codiceOfferta=cod;
	}
	public int getCodiceAsta(){
		
		return codiceAsta;
	}

}