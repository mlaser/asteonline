package asteOnline.client;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface LoginRegisterServiceAsync {
	
	void register(String input[], AsyncCallback<String> callback);
	void login(String input[], AsyncCallback<String> callback);
	void infoUser(String input, AsyncCallback<Utente> callback);
	void adminReg(AsyncCallback<Void> callback);
	
}
