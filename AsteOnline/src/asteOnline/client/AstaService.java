package asteOnline.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("aste")
public interface AstaService extends RemoteService{

	int saveAsta(Asta oggAsta);

	ArrayList<Asta> loadAsta(int cat);

	String inserisciDomanda(Domanda input);

	ArrayList<Domanda> loadDomande(int input);

	void deleteDomanda(int input);

	String inserisciRisposta(Domanda input);

	ArrayList<Offerta> loadOfferta(int codiceAsta);

	int saveOfferta(asteOnline.client.Offerta offer);

	void elimnaOfferta(int codiceOfferta);

	void elimnaAsta(int codiceAsta);

	ArrayList<Asta> loadAstaUtente(String user);

	ArrayList<Offerta> loadOffertaUtente(String input);

	String descrAsta(int input);

	String statoOfferta(int input);



}