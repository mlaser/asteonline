package asteOnline.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;



@RemoteServiceRelativePath("categorie")
public interface CategoriaService extends RemoteService {
	void saveCategoria(Categoria utente);
	ArrayList<Albero> loadCategoria();
	void renameCategoria(Albero nodoCat, String nuovoNome);
}
