package asteOnline.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface AstaServiceAsync {

	

	void saveAsta(Asta oggAsta, AsyncCallback<Integer> asyncCallback);

	

	void loadAsta(int cat, AsyncCallback<ArrayList<Asta>> asyncCallback);



	void inserisciDomanda(Domanda input, AsyncCallback<String> callback);



	void loadDomande(int input, AsyncCallback<ArrayList<Domanda>> callback);



	void deleteDomanda(int input, AsyncCallback<Void> callback);



	void inserisciRisposta(Domanda input, AsyncCallback<String> callback);



	void loadOfferta(int codiceAsta, AsyncCallback<ArrayList<Offerta>> callback);



	void saveOfferta(Offerta offer, AsyncCallback<Integer> callback);



	void elimnaOfferta(int codiceOfferta, AsyncCallback<Void> asyncCallback);



	void elimnaAsta(int codiceAsta, AsyncCallback<Void> asyncCallback);



	void loadAstaUtente(String user, AsyncCallback<ArrayList<Asta>> callback);



	void loadOffertaUtente(String input, AsyncCallback<ArrayList<Offerta>> callback);



	void descrAsta(int input, AsyncCallback<String> callback);



	void statoOfferta(int input, AsyncCallback<String> callback);

	


}