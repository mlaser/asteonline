package asteOnline.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("registerLogin")
public interface LoginRegisterService extends RemoteService {
	
	String register(String name[]);
	String login(String name[]);
	Utente infoUser(String input) throws IllegalArgumentException;
	void adminReg() throws IllegalArgumentException;
	
}
