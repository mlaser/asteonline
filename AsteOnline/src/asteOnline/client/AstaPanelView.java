package asteOnline.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.RootPanel;

public class AstaPanelView implements Serializable {
	private static final long serialVersionUID = 1L;
	private int categoria;
	private AstaServiceAsync astaSvc = GWT.create(AstaService.class);
	public AstaPanelView(int categoria){
		this.categoria=categoria;
		
	} 
	
	private void eliminaAsta(int codiceAsta){
		astaSvc.elimnaAsta(codiceAsta, new AsyncCallback<Void>(){

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Asta eliminata");
				creaPanel("admin");
			}
			
		});
	}
	
	public AstaPanelView(){
		
	}
	
	public void creaPanel(String user){
		final String utente=user;
		astaSvc.loadAsta(categoria,new AsyncCallback<ArrayList<Asta>>() {
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore!");
			}
			
			@Override
			public void onSuccess(ArrayList<Asta> result) {
				
				ArrayList<Asta> result1=comparaAsta(result);
				Collections.reverse(result1);
				FlexTable t = new FlexTable();
				RootPanel.get("livello1").clear();
				RootPanel.get("livello2").clear();
				RootPanel.get("livello3").clear();
				t.setTitle("ARTICOLI");
			    t.setText(0, 0, "DESCRIZIONE");
			    t.setText(0, 1, "SCADENZA");
			    t.setText(0, 2, "PROPRIETARIO");
			    t.setText(0, 3, "PREZZO BASE");
			    t.setWidth("300px");
			    t.setCellSpacing(50);
			    int j=1;
				for(int i=0;i<result.size();i++){
					Date today = new Date(); //Prendo la data di oggi
					//Inizio operazioni per ricavare la data di scadenza dell'asta da inserire nella tabella
					int anno=Integer.parseInt(result1.get(i).getDataScadenza().substring(0,4));
					int mese=Integer.parseInt(result1.get(i).getDataScadenza().substring(5,7));
					int giorno=Integer.parseInt(result1.get(i).getDataScadenza().substring(8,10));
					//Ricavo giorno, mese e anno della data odierna
					int giornoOggi = today.getDate();
					int meseOggi=today.getMonth()+1;
					int annoOggi=today.getYear()+1900;
					//Confronto la data di oggi con la data di scadenza per verificare che l'asta non sia scaduta e la inserisco nella tabella se questa non � scaduta
					if(anno>annoOggi||(anno==annoOggi&&mese>meseOggi)||(anno==annoOggi&&mese==meseOggi&&giorno>giornoOggi)||(utente.equals("admin"))) {
						Button dettagli=new Button("Dettagli");
						Button elimina=new Button("Elimina");
						t.setText(j, 0, result1.get(i).getDescrizione());
						t.setText(j, 1, result1.get(i).getDataScadenza());
						t.setText(j, 2, result1.get(i).getProprietario());
						t.setText(j, 3,""+result1.get(i).getPrezzo());
						final String descrizione=result1.get(i).getDescrizione();
						final String scadenza=result1.get(i).getDataScadenza();
						final String proprietario=result1.get(i).getProprietario();
						final double prezzo=result1.get(i).getPrezzo();
						final int codiceAsta=result1.get(i).getCodiceAsta();
						t.setWidget(j,4,dettagli);
						if(utente.equals("admin")){ 
							t.setWidget(j,5,elimina);
							elimina.addClickHandler(new ClickHandler(){
								@Override
								public void onClick(ClickEvent event) {
									//Elimina Asta
									eliminaAsta(codiceAsta);
								}
							});
						}
						
						dettagli.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								DescrizioneAsta descrAsta=new DescrizioneAsta(descrizione,scadenza,proprietario,prezzo,codiceAsta);
								descrAsta.creaPanel(utente);
							}
						});
						j++;
					} 
				}RootPanel.get("livello1").add(t);
			}			
		});		
	}

	public ArrayList<Asta> comparaAsta(ArrayList<Asta> aste){


		for(int i=0;i<aste.size();i++){
			int annoMax=0;
			int meseMax=0;
			int giornoMax=0;
			int pos=0;
			for(int j=i;j<aste.size();j++){
				int anno=Integer.parseInt(aste.get(j).getDataScadenza().substring(0,4));
				int mese=Integer.parseInt(aste.get(j).getDataScadenza().substring(5,7));
				int giorno=Integer.parseInt(aste.get(j).getDataScadenza().substring(8,10));
				if((anno>annoMax)||(anno==annoMax&&mese>meseMax)||(anno==annoMax&&mese==meseMax&&giorno>giornoMax)){
					annoMax=anno;
					meseMax=mese;
					giornoMax=giorno;
					pos=j;
				}

			}
			Asta tmp1=aste.get(pos);
			Asta tmp2=aste.get(i);
			aste.set(i, tmp1);
			aste.set(pos,tmp2);

		}
		return aste;
	}
}
