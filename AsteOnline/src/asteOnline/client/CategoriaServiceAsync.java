package asteOnline.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;



public interface CategoriaServiceAsync {
	
	void saveCategoria(Categoria utente, AsyncCallback<Void> callback);
	

	void loadCategoria(AsyncCallback<ArrayList<Albero>> callback);

	void renameCategoria(Albero nodoCat, String nuovoNome, AsyncCallback<Void> callback);
}
