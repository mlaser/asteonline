package asteOnline.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;

public class RispostaPanel {
	private AstaServiceAsync astaSvc = GWT.create(AstaService.class);
	
	final Button rispondiButton= new Button("Invia");
	final TextArea risposta = new TextArea();
	
    public RispostaPanel(){
    	
    	
    }
    public void creaPanel(Domanda element, String user){
    	final Domanda domandaEl = element;
    	RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
    	risposta.setCharacterWidth(80);
        risposta.setVisibleLines(10);
        risposta.getElement().setAttribute("style", "resize:none");
    	HTML domanda = new HTML("<b>Domanda: </b>"+element.getDomanda()+" <b>Scritta da: </b>"+element.getUtente()+"<br><br><b>Inserisci la risposta:</b>");
    	risposta.getElement().setAttribute("style", "margin-top:-25px");
    	RootPanel.get("livello1").add(domanda);
    	RootPanel.get("livello2").add(risposta);
    	RootPanel.get("livello3").add(rispondiButton);
    	
    	rispondiButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//TODO Rispondi
				domandaEl.setRisposta(risposta.getText());
				astaSvc.inserisciRisposta(domandaEl, new AsyncCallback<String>(){
					@Override
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}
					@Override
					public void onSuccess(String result) {
						Window.alert("Risposta inseirta con successo!");
						RootPanel.get("livello1").clear();
				    	RootPanel.get("livello2").clear();
				    	RootPanel.get("livello3").clear();
					}
				});
			}
		});
    }
}