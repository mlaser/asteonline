package asteOnline.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;

public class DomandeRispostePanel {
		
	private AstaServiceAsync astaSvc = GWT.create(AstaService.class);
	
	final TextArea domanda = new TextArea();
	
    public DomandeRispostePanel(){
    	
    	
    }
        
    public void creaPanel(String utente, int codiceA){
    	final String user=utente;
    	final int codiceAsta = codiceA;
    	RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
    	Label prova = new Label("Inserisci una Domanda");
    	RootPanel.get("livello1").add(prova);
    	
        domanda.setCharacterWidth(80);
        domanda.setVisibleLines(10);
        domanda.getElement().setAttribute("style", "resize:none");
        RootPanel.get("livello1").add(domanda);
        Button domandaButton = new Button("Invia");
        RootPanel.get("livello2").add(domandaButton);
        
        //Inserimento domanda
        domandaButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//TODO Domanda
				String testoDomanda=domanda.getText();
				Domanda newDomanda = new Domanda(codiceAsta, user, testoDomanda);
				
				astaSvc.inserisciDomanda(newDomanda, new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						Window.alert(caught.toString());
					}

					public void onSuccess(String result) {
						domanda.setText(null);
						RootPanel.get("livello1").clear();
				    	RootPanel.get("livello2").clear();
				    	RootPanel.get("livello3").clear();
				    	Window.alert("Domanda inserita con successo!");
					}
				});
			}
		});
        //Fine inserimento domanda 
    }
}