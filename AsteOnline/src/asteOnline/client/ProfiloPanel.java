package asteOnline.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public class ProfiloPanel {
	private LoginRegisterServiceAsync loginRegSvc = GWT.create(LoginRegisterService.class);
	private AstaServiceAsync astaSvc = GWT.create(AstaService.class);
	FlexTable t1 = new FlexTable();
	
    public ProfiloPanel(){
    	
    	
    }
    
    private void visualizzaAste(String user){
    	astaSvc.loadAstaUtente(user,new AsyncCallback<ArrayList<Asta>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore!");
			}
			
			@Override
			public void onSuccess(ArrayList<Asta> result) {
				Label lb=new Label("Aste create:");
				lb.getElement().getStyle().setFontSize(20, Unit.PX);
				RootPanel.get("livello2").add(lb);
				if(result.size()==0){
					
					 Label prezz=new Label("Nessuna asta inserita!");
					 prezz.getElement().getStyle().setFontSize(20, Unit.PX);
					 RootPanel.get("livello3").add(prezz);
					
				}else{
				
					FlexTable t = new FlexTable();
					t.setTitle("ARTICOLI");
					t.setText(0, 0, "DESCRIZIONE");
					t.setText(0, 1, "SCADENZA");
					t.setText(0, 2, "PROPRIETARIO");
					t.setText(0, 3, "PREZZO(€)");
					t.setText(0, 4, "STATO");
					Date today = new Date();
					DateTimeFormat fmt = DateTimeFormat.getFormat("yyyy-MM-dd");
					fmt.format(today);
					t.setWidth("300px");
					t.setCellSpacing(50);
					for(int i=0;i<result.size();i++){
						final String user1 = result.get(i).getProprietario();
						Button dettagli=new Button("Dettagli");
						t.setText(i+1, 0, result.get(i).getDescrizione());
						t.setText(i+1, 1, result.get(i).getDataScadenza());
						t.setText(i+1, 2, result.get(i).getProprietario());
						t.setText(i+1, 3,""+result.get(i).getPrezzo());
				   
						DateTimeFormat format = DateTimeFormat.getFormat("yyyy-MM-dd");
						Date data = format.parse(result.get(i).getDataScadenza());
						if(today.before(data))t.setText(i+1, 4,"In Corso");
						else t.setText(i+1, 4,"Conclusa");
				    
						final String descrizione=result.get(i).getDescrizione();
						final String scadenza=result.get(i).getDataScadenza();
						final String proprietario=result.get(i).getProprietario();
						final double prezzo=result.get(i).getPrezzo();
						final int codiceAsta=result.get(i).getCodiceAsta();
						t.setWidget(i+1,5,dettagli);
						dettagli.addClickHandler(new ClickHandler() {
							@Override
							public void onClick(ClickEvent event) {
								DescrizioneAsta descrAsta=new DescrizioneAsta(descrizione,scadenza,proprietario,prezzo,codiceAsta);
								descrAsta.creaPanel(user1);
							}
						});			
					}
				RootPanel.get("livello2").add(t);
				}
			}
		});
    }
    
    private void caricaNomeAsta(int codAsta, int index){
    	final int temp = index;
    	astaSvc.descrAsta(codAsta,new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore!");
			}
			
			@Override
			public void onSuccess(String result) {
					t1.setText(temp, 0, result);
				}
		});
    }
    
    //Carica stato offerta
    private void caricaStatoOfferta(int codOff, int index){
    	final int temp = index;
    	astaSvc.statoOfferta(codOff,new AsyncCallback<String>() {
    				@Override
    				public void onFailure(Throwable caught) {
    					Window.alert(caught.getMessage());
    				}

					@Override
					public void onSuccess(String result) {
						t1.setText(temp, 2, result);
					}
    			});
    }
    
    //Carica offerte
    private void visualizzaOfferte(String user){
    	astaSvc.loadOffertaUtente(user,new AsyncCallback<ArrayList<Offerta>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore!");
			}
			@Override
			public void onSuccess(ArrayList<Offerta> result) {
				Label lb=new Label("Offerte fatte:");
				lb.getElement().getStyle().setFontSize(20, Unit.PX);
				RootPanel.get("livello3").add(lb);
				if(result.size()==0){
					 Label prezz=new Label("Nessuna offerta inserita!");
					 prezz.getElement().getStyle().setFontSize(20, Unit.PX);
					 RootPanel.get("livello3").add(prezz);
				}else{
					t1.setTitle("OFFERTE");
				    t1.setText(0, 0, "DESCRIZIONE");
				    t1.setText(0, 1, "IMPORTO OFFERTO(€)");
				    t1.setText(0, 2, "STATO OFFERTA");
				    t1.setWidth("300px");
				    t1.setCellSpacing(50);
				    Collections.reverse(result);
					for(int i=0;i<result.size();i++){
						caricaNomeAsta(result.get(i).getCodiceAsta(), i+1);
					    t1.setText(i+1, 1,""+ result.get(i).getImporto());
					    caricaStatoOfferta(result.get(i).getCodiceOfferta(), i+1);    
					}
					RootPanel.get("livello3").add(t1);
				}
			}
		});
    }
    
    public void creaPanel(String user){
    	RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
    	Label lbProfilo = new Label("Profilo");
    	lbProfilo.getElement().setAttribute("style", "font-size:21pt; margin-bottom:5pt");
    	RootPanel.get("livello1").add(lbProfilo);
    	//Carico dati di registrazione utente
    	loginRegSvc.infoUser(user, new AsyncCallback<Utente>() {
			public void onFailure(Throwable caught) {
				Window.alert(caught.toString());
			}

			public void onSuccess(Utente result) {
				HTML datiUser = new HTML("<b>Username: </b>"+result.getUsername()+
						"<br><br><b>Nome: </b>"+result.getNome()+
						"<br><br><b>Cognome: </b>"+result.getCognome()+
						"<br><br><b>Numero di telefono: </b>"+result.getNumeroTelefono()+
						"<br><br><b>Email: </b>"+result.getEmail()+
						"<br><br><b>Codice Fiscale: </b>"+result.getCodiceFiscale()+
						"<br><br><b>Domicilio: </b>"+result.getDomicilio());
				HTML datiUserSesso = new HTML("<br><b>Sesso: </b>"+result.getSesso());
				HTML datiUserDataN = new HTML("<br><b>Data di nascita: </b>"+result.getDataNascita());
				HTML datiUserLuogoN = new HTML ("<br><b>Luogo di nascita: </b>"+result.getLuogoNascita());
		    	RootPanel.get("livello1").add(datiUser);
		    	if(!result.getSesso().equals("")) RootPanel.get("livello1").add(datiUserSesso);
		    	if(!result.getDataNascita().equals("")) RootPanel.get("livello1").add(datiUserDataN);
		    	if(!result.getLuogoNascita().equals("")) RootPanel.get("livello1").add(datiUserLuogoN);
		    	visualizzaAste(result.getUsername());
		    	visualizzaOfferte(result.getUsername());
			}
		}); 	
    } 
}