package asteOnline.client;

import java.io.Serializable;

public class Utente implements Serializable{
	
	private static final long serialVersionUID = 6220340462331804675L;
	private String username;
	private String password;
	private String nome;
	private String cognome;
	private String numeroTelefono;
	private String email;
	private String codiceFiscale;
	private String domicilio;
	private String sesso;
	private String dataNascita;
	private String luogoNascita;
	
	private boolean admin=false;
	
	// Costruttore con campi obbligatori
	public Utente(String username, String password, String nome, String cognome, String numeroTelefono,
			String email, String codiceFiscale, String domicilio){
		
		this.username=username;
		this.password=password;
		this.cognome=cognome;
		this.nome=nome;
		this.numeroTelefono=numeroTelefono;
		this.email=email;
		this.codiceFiscale=codiceFiscale;
		this.domicilio=domicilio;
	}
	
	// Costruttore per amministratore
	public Utente(){
		
		this.username="admin";
		this.password="admin";		
		setAdmin();
	}
	
	// SETTER
	
	public void setSesso(String sesso){		
		this.sesso=sesso;
	}
	
	public void setDataNascita(String dataNascita){		
		this.dataNascita=dataNascita;
	}
	
	public void setLuogoNascita(String luogoNascita){		
		this.luogoNascita=luogoNascita;
	}
	
	public void setAdmin(){
		this.admin=true;
	}
	
	
	// GETTER
	
	public String getUsername(){
		return username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getNome(){
		return nome;
	}
	
	public String getCognome(){
		return cognome;
	}
	
	public String getNumeroTelefono(){
		return numeroTelefono;
	}
	
	public String getEmail(){
		return email;
	}
	
	public String getCodiceFiscale(){
		return codiceFiscale;
	}
	
	public String getDomicilio(){
		return domicilio;
	}
	
	public String getSesso(){
		return sesso;
	}
	
	public String getDataNascita(){
		return dataNascita;
	}
	
	public String getLuogoNascita(){
		return luogoNascita;		
	}
	
}
