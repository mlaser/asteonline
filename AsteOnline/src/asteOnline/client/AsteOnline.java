package asteOnline.client;

import java.util.ArrayList;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

public class AsteOnline implements EntryPoint {
	private CategoriaServiceAsync categoriaSvc = GWT.create(CategoriaService.class);
	private LoginRegisterServiceAsync loginRegSvc = GWT.create(LoginRegisterService.class);
	
	TreeItem root=new TreeItem();
	Tree t = new Tree();
	ArrayList<Albero> alberoCategorie;
	Button nuovaSottocategoria= new Button("Crea Sottocategoria");;
	Button rinomina = new Button("Rinomina");
	Button nuovaCategoria = new Button("Crea Categoria");	
	ListBox listaCat=new ListBox();
	ListBox listaCat1=new ListBox();
	final TextBox nuovaCatTesto = new TextBox();
	final TextBox nuovaSottocatTesto = new TextBox();
    final TextBox rinominaCatTesto = new TextBox();	
    Anchor gestisciCategorie = new Anchor("Gestione Categorie");
	Anchor registraClick = new Anchor("Registrati");
	Anchor loginClick = new Anchor("Login");
	Anchor profiloClick = new Anchor("Profilo");
	Anchor logoutClick = new Anchor("Logout");
	Anchor creaAsta = new Anchor("Crea Asta");
	int count=0;
	String session=null;
	
	public void onModuleLoad() {
		RootPanel.get("navPanel").clear();
		RootPanel.get("navPanel").add(registraClick);
		RootPanel.get("navPanel").add(loginClick);
		caricaAlbero();
		caricaHandler();
		alberoHandler(session);
		//Richiamo metodo per creazione utente di default con privilegi admin
		
		loginRegSvc.adminReg( new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				
			}
			@Override
			public void onSuccess(Void result) {
				
			}
			
		});
		//Fine richiamo metodo per creazione utente di default con privilegi admin
		
		//Handler per pagina di registrazione
		registraClick.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//TODO Registrazione
				RegistraPanel registerPanel = new RegistraPanel();
				registerPanel.creaPanel();
			}
		});
		//Fine handler per pagina di registrazione
		
		//Handler per pagina di login
		loginClick.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//TODO Login
				LoginPanel loginPanel = new LoginPanel();
				loginPanel.creaPanel();
			}
		});
		//Fine handler per pagina di login
	}
	
	//Inizio operazioni di formattazione navbar a login effettuato
	public void effettuaLogin(String utente){
		RootPanel.get("navPanel").clear();
		session=utente;
		RootPanel.get("albero").clear();
		caricaAlbero();
		
		alberoHandler(session);
		//Controllo che l'utente che ha effettuato il login NON sia l'admin, quindi aggiungo l'anchor per creare un asta
		if(!session.equals("admin")) RootPanel.get("navPanel").add(creaAsta);
		creaAsta.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				listaCat=new ListBox();
				RootPanel.get("albero").clear();
				caricaAlbero();
				caricaHandler();
			
				AstaPanel pannello=new AstaPanel(listaCat);
				pannello.creaPanel(session);
			}
		});
		if(session.equals("admin")) { //Controllo per privilegi admin
		
			RootPanel.get("navPanel").add(gestisciCategorie);
			caricaHandler();
			
			gestisciCategorie.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					listaCat=new ListBox();
					listaCat1=new ListBox();
					RootPanel.get("albero").clear();
					caricaAlbero();
					pannelloCategorie();
				
				}
			});
			
			
			RootPanel.get("navPanel").add(logoutClick);
			logoutClick.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					session=null;
					RootPanel.get("livello1").clear();
			    	RootPanel.get("livello2").clear();
			    	RootPanel.get("livello3").clear();
			    	RootPanel.get("navPanel").clear();
			    	RootPanel.get("albero").clear();
					onModuleLoad();
				
				}
			});
			
		} else RootPanel.get("navPanel").add(profiloClick);
		RootPanel.get("navPanel").add(logoutClick);
		RootPanel.get("livello1").clear();
    	RootPanel.get("livello2").clear();
    	RootPanel.get("livello3").clear();
				
		logoutClick.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				session=null;
				Window.alert("Logout effettuato con successo");
				RootPanel.get("livello3").clear();
				RootPanel.get("livello1").clear();
		    	RootPanel.get("livello2").clear();
		    	RootPanel.get("livello3").clear();
		    	RootPanel.get("navPanel").clear();
		    	RootPanel.get("albero").clear();
				onModuleLoad();
			
			}
		});
		
		profiloClick.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				ProfiloPanel profilePanel = new ProfiloPanel();
				profilePanel.creaPanel(session);
			}
		});
	}
	//Fine operazioni di formattazione navbar a login effettuato
	
	//Procedura creazione albero
	public void caricaAlbero(){
		
		categoriaSvc.loadCategoria(new AsyncCallback<ArrayList<Albero>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Cannot load symbols: "
						+ caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<Albero> result) {
				alberoCategorie=new ArrayList<Albero>();
				alberoCategorie=result;
				for(int i=0;i<result.size();i++){
					
					listaCat.insertItem(result.get(i).getCategoria().getNome(),result.get(i).getId());
					listaCat1.insertItem(result.get(i).getCategoria().getNome(),result.get(i).getId());
				}
				Albero radice=new Albero(0,new Categoria("Categorie",-1));
				
				t.clear();
				root=new TreeItem();
				root.setUserObject(radice);
				root.setText("Tutti gli Articoli");
				 t.addItem(root);
				createTree(result,root,0,-1);
				RootPanel.get("albero").clear();
				RootPanel.get("albero").add(t);
			}
		});
	}
	
	public void pannelloCategorie(){
		Label lb=new Label("CREA NUOVA CATEGORIA:");
		Label lb1=new Label("CREA NUOVA SOTTOCATEGORIA:");
		Label lb2=new Label("RINOMINA:");

		RootPanel.get("livello1").clear();
		RootPanel.get("livello2").clear();
		RootPanel.get("livello3").clear();
		RootPanel.get("livello1").add(lb);
		RootPanel.get("livello1").add(nuovaCatTesto);
		RootPanel.get("livello1").add(nuovaCategoria);
		RootPanel.get("livello2").add(lb1);
		RootPanel.get("livello2").add(nuovaSottocatTesto);
		RootPanel.get("livello2").add(listaCat);
		RootPanel.get("livello2").add(nuovaSottocategoria);
		RootPanel.get("livello3").add(lb2);
		RootPanel.get("livello3").add(rinominaCatTesto);
		RootPanel.get("livello3").add(listaCat1);
		RootPanel.get("livello3").add(rinomina);
		
		
	}
	
	
	public void createTree(ArrayList<Albero> categorie,TreeItem currentParent, int currLevel , int prevLevel){
	     Albero padre=(Albero) currentParent.getUserObject();
	     
		  //per ogni elemento nell'array
	      for ( int i=0; i<categorie.size(); i++) {
	         //se � figlio dell'elemento esaminato 
	         if (padre.getId() == categorie.get(i).getCategoria().getPadre()) {
	        	TreeItem child = new TreeItem();
	            child.setUserObject(categorie.get(i));
	            child.setText(categorie.get(i).getCategoria().getNome());
	            Albero figlio=(Albero) child.getUserObject();
	            if(padre.getId()==(figlio.getCategoria().getPadre())){
	        		currentParent.addItem(child);        	 
	        	}
	            // se il livello corrente � maggiore prevLevel diventa = a curr level; 
	            if (currLevel > prevLevel) { 
	            	prevLevel = currLevel; 
	            }
	             currLevel++;
	            // rilancio l'algoritmo ricorsivamente 
	            createTree (categorie, child, currLevel, prevLevel);
		        currLevel--;               
	         }
	      } 
	   }   

	  public void caricaHandler(){
			nuovaSottocategoria.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String nome;
					int padre;
					final Categoria categoria;
					nome = nuovaSottocatTesto.getText().toUpperCase().trim();
					padre = listaCat.getSelectedIndex();
					categoria=new Categoria(nome,padre+1);

					categoriaSvc.saveCategoria(categoria, new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Cannot load symbols: "
									+ caught.getMessage());
						}

						@Override
						public void onSuccess(Void result) {
							Window.alert("Categoria creata con successo!");
							listaCat=new ListBox();
							listaCat1=new ListBox();
							nuovaSottocatTesto.setText("");
							caricaAlbero();
							pannelloCategorie();
						}
					});
				}
			});
					
			nuovaCategoria.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String nome;
					int padre;
					final Categoria categoria;
					nome = nuovaCatTesto.getText().toUpperCase().trim();
					padre = 0;
					categoria=new Categoria(nome,padre);

					categoriaSvc.saveCategoria(categoria, new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Cannot load symbols: "
									+ caught.getMessage());
						}

						@Override
						public void onSuccess(Void result) {
							Window.alert("Categoria creata con successo!");
							listaCat=new ListBox();
							listaCat1=new ListBox();
							nuovaCatTesto.setText("");
							caricaAlbero();
							pannelloCategorie();
						}
					});
				}
			});
									
			rinomina.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String nuovoNome;
					int padre;
					nuovoNome = rinominaCatTesto.getText().toUpperCase().trim();
					padre = listaCat1.getSelectedIndex();
					Albero nodoCat=alberoCategorie.get(padre);
									
					categoriaSvc.renameCategoria(nodoCat,nuovoNome,	new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Cannot load symbols: "
									+ caught.getMessage());
						}

						@Override
						public void onSuccess(Void result) {
							Window.alert("Categoria rinominata con successo!");
							listaCat=new ListBox();
							listaCat1=new ListBox();
							rinominaCatTesto.setText("");
							caricaAlbero();
							pannelloCategorie();
						}
					});
				}
			});
	  }
	  
	  public void alberoHandler(String utente){
		  count++;
		  t.addSelectionHandler(new SelectionHandler<TreeItem>() {
				
			  @Override
			  public void onSelection(SelectionEvent<TreeItem> event) {
				  TreeItem item = event.getSelectedItem();			   
				  Albero valore=(Albero)item.getUserObject();
				  AstaPanelView view=new AstaPanelView(valore.getId());
				  view.creaPanel(session);
				   
			  }
		  });
	  }	  
}

	
