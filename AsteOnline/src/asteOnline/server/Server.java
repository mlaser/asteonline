package asteOnline.server;
import java.io.File;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import asteOnline.client.Albero;
import asteOnline.client.Categoria;
import asteOnline.client.CategoriaService;

public class Server extends RemoteServiceServlet implements CategoriaService{
	private static final long serialVersionUID = 1L;
	
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
			}
			return db;
		}
	}

	@Override
	public void saveCategoria(Categoria categoria) {
		DB db = getDB();
		Map<Integer, Categoria> map = db.getTreeMap("categoria");
		int max=0;
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(key>max){
				max=key;
			}
		}
		map.put(max+1, categoria);
		db.commit();
	}

	@Override
	public ArrayList<Albero> loadCategoria() {
		DB db = getDB();
		Map<Integer, Categoria> map = db.getTreeMap("categoria");
		Albero oggAlb;
		ArrayList<Albero> albero=new ArrayList<Albero>();
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			oggAlb=new Albero(key,map.get(key));
			albero.add(oggAlb);
		}
		return albero;
	}

	@Override
	public void renameCategoria(Albero nodoCat, String nuovoNome) {
		DB db = getDB();
		Map<Integer, Categoria> map = db.getTreeMap("categoria");
		Categoria nuovaCat=new Categoria(nuovoNome,nodoCat.getCategoria().getPadre());
		map.remove(nodoCat.getId(), nodoCat.getCategoria());
		map.put(nodoCat.getId(), nuovaCat);
		db.commit();
	}
}