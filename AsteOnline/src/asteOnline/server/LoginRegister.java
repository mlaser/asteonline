package asteOnline.server;

import asteOnline.client.LoginRegisterService;
import asteOnline.client.Utente;
import java.io.File;
import java.util.Map;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


@SuppressWarnings("serial")
public class LoginRegister extends RemoteServiceServlet implements LoginRegisterService {
	//Database
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
			}
			return db;
		}
	}
	
	//Registrazione
	
	
	public int checkRegistrazione(String input){
		DB users = getDB();
		Map <String, Utente> myMap = users.getTreeMap("utenti");
		Utente value = myMap.get(input);
		if (value!=null){
			String cntrl = value.getUsername();
			if(input.equals(cntrl)){
				users.commit();
				return 1;
			}
		}
		users.commit();
		return 0;
	}
	
	//Admin
	@Override
	public void adminReg() throws IllegalArgumentException {
		Utente admin = new Utente();
		if(checkRegistrazione(admin.getUsername())==0){
			DB users = getDB();
			Map <String, Utente> myMap = users.getTreeMap("utenti");
			myMap.put(admin.getUsername(), admin);
			users.commit();
		}
	}
	
	@Override
	public String register(String input[]) throws IllegalArgumentException {

		if(checkRegistrazione(input[0])==0){
			for(int i=0; i<8; i++){
				if(input[i]==null) return "Campi mancanti, compilare tutti i campi obbligatori!";
			}
			DB users = getDB();
			Utente newUser=new Utente(input[0],input[1],input[2],input[3],input[4],
				input[5],input[6],input[7]);
			if(input[8]!=null) newUser.setSesso(input[8]); //Controllo e inserimento per campo opzionale Sesso
			if(input[9]!=null) newUser.setDataNascita(input[9]); //Controllo e inserimento per campo opzionale Data di Nascita
			if(input[10]!=null) newUser.setLuogoNascita(input[10]); //Controllo e inserimento per campo opzionale Luogo di Nascita
			Map <String, Utente> myMap = users.getTreeMap("utenti");
			myMap.put(input[0], newUser);
			Utente value = myMap.get(input[0]);
			users.commit();
			return "Utente "+value.getUsername()+" registrato con successo, i dati inseriti sono i seguenti: \n Nome: "+value.getNome()+"\n Cognome: "+value.getCognome()+"\n Numero di Telefono "+value.getNumeroTelefono()+"\n Email "
					+value.getEmail()+"\n Codice Fiscale: "+value.getCodiceFiscale()+"\n Domicilio: "+value.getDomicilio()+"\n Sesso: "+value.getSesso()+"\n Data di nascita: "+value.getDataNascita()+"\n Luogo di nascita: "+value.getLuogoNascita();
		}else return "Utente gi� registrato!";
	}
	
	//Login
	@Override
	public String login(String input[]) throws IllegalArgumentException {
		DB users = getDB();
		Map <String, Utente> myMap = users.getTreeMap("utenti");
		Utente value = myMap.get(input[0]);
		if(value!=null){
			if(value.getPassword().equals(input[1])) {
				return input[0];
			} else return "Password errata";
		} else return "Utente non esistente";
	}
	
	//Info User
	@Override
	public Utente infoUser(String input) throws IllegalArgumentException{
		DB users = getDB();
		Map <String, Utente> myMap = users.getTreeMap("utenti");
		Utente value = myMap.get(input);
		return value;
	}
	
}

