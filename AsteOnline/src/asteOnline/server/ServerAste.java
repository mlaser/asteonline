package asteOnline.server;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import asteOnline.client.Asta;
import asteOnline.client.AstaService;
import asteOnline.client.Domanda;
import asteOnline.client.Offerta;



public class ServerAste extends RemoteServiceServlet implements AstaService {
	private static final long serialVersionUID = 1L;
	private DB getDB() {
		ServletContext context = this.getServletContext();
		synchronized (context) {
			DB db = (DB)context.getAttribute("DB");
			if(db == null) {
				db = DBMaker.newFileDB(new File("db")).closeOnJvmShutdown().make();
				context.setAttribute("DB", db);
			}
			return db;
		}
	}
	@Override
	public int saveAsta(Asta oggAsta) {
		DB db = getDB();
		Map<Integer, Asta> map = db.getTreeMap("asta");
		int max=0;
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(key>max){
				max=key;
			}
		}
		
		oggAsta.setCodiceAsta(max+1);
		map.put(max+1, oggAsta);
		db.commit(); 
		return 1;
	}
	
	@Override
		public ArrayList<Asta> loadAsta(int cat) {
			DB db = getDB();
			Map<Integer, Asta> map = db.getTreeMap("asta");
			
			ArrayList<Asta> listaAste=new ArrayList<Asta>();
			Set<Integer> keys = map.keySet();
			for(int key : keys) {
				for(int i=0;i<map.get(key).getCategoria().size();i++){
					if(map.get(key).getCategoria().get(i)==cat){
						listaAste.add(map.get(key));
					}
				}
				
			}
			return listaAste;
	}
	
	@Override
	public ArrayList<Offerta> loadOfferta(int codiceAsta) {
		DB db = getDB();
		Map<Integer, Offerta> map = db.getTreeMap("offerta");
		
		ArrayList<Offerta> listaOfferte=new ArrayList<Offerta>();
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(codiceAsta==map.get(key).getCodiceAsta())
				listaOfferte.add(map.get(key));
		}
		return listaOfferte;
	}
	@Override
	public int saveOfferta(Offerta offer) {
		DB db = getDB();
		Map<Integer, Offerta> map = db.getTreeMap("offerta");
		int max=0;
		double maxOfferta=0;
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(key>max){
				max=key;
			}
			if(map.get(key).getCodiceAsta()==offer.getCodiceAsta()){
				if(map.get(key).getImporto()>maxOfferta){
					maxOfferta=map.get(key).getImporto();	
				}
			}
		}
		if(offer.getImporto()>maxOfferta){
		offer.setCodiceOfferta(max+1);
		map.put(max+1, offer);
		db.commit();
		return 1;
		}else{
			return 0;
		}
	}
	
	//Inserimento nuova Domanda
	@Override
	public String inserisciDomanda(Domanda input){
		DB db = getDB();
		Map<Integer, Domanda> map = db.getTreeMap("domanda");
		int max=0;		
		Set<Integer> keys = map.keySet();
		for(int key : keys) {			
			if(key>max){
				max=key;				
			}
		}
		input.setCodiceDomanda(max+1);
		map.put(max+1, input);
		Domanda inserita = map.get(max+1);
		db.commit();
		String testo = inserita.getDomanda();
		String user = inserita.getUtente();
		int codice = inserita.getCodiceAsta();
		return testo+" "+user+" "+codice;
	}
	
	//Inserimento risposta
	@Override
	public String inserisciRisposta(Domanda input){
		DB db = getDB();
		Map<Integer, Domanda> map = db.getTreeMap("domanda");
		map.put(input.getCodiceDomanda(), input);
		String risposta = map.get(input.getCodiceDomanda()).getRisposta();
		db.commit();
		return risposta;
	}
	
	//Prendi tutte le domande
	@Override
	public ArrayList<Domanda> loadDomande(int input){
		ArrayList<Domanda> listaDomande=new ArrayList<Domanda>();
		DB db = getDB();
		Map<Integer, Domanda> map = db.getTreeMap("domanda");
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
			if(map.get(key).getCodiceAsta()==input) listaDomande.add(map.get(key));
		}
		return listaDomande;
	}
	
	//Elimina domanda
	@Override
	public void deleteDomanda(int input){
		DB db = getDB();
		Map<Integer, Domanda> map = db.getTreeMap("domanda");
		map.remove(input);
		db.commit();
	}
	//Elimina Offerta
	@Override
	public void elimnaOfferta(int codiceOfferta) {
		DB db = getDB();
		Map<Integer, Offerta> map = db.getTreeMap("offerta");
		map.remove(codiceOfferta);
		db.commit();
		
	}
	//Elimina Asta
	@Override
	public void elimnaAsta(int codiceAsta) {
		DB db = getDB();
		Map<Integer, Asta> map = db.getTreeMap("asta");
		DB db1 = getDB();
		Map<Integer, Offerta> map1 = db1.getTreeMap("offerta");
		DB db2 = getDB();
		Map<Integer, Domanda> map2 = db2.getTreeMap("domanda");
		map.remove(codiceAsta);
		Set<Integer> keys = map1.keySet();
		for(int key : keys) {
			if(map1.get(key).getCodiceAsta()==codiceAsta) map1.remove(key);
		}
		Set<Integer> keys2 = map2.keySet();
		for(int key : keys2) {
			if(map2.get(key).getCodiceAsta()==codiceAsta) map2.remove(key);
		}
		db.commit();
		db1.commit();
		db2.commit();
	}
	
	//Carica aste per utente
	@Override
	public ArrayList<Asta> loadAstaUtente(String input) {
		DB db = getDB();
		Map<Integer, Asta> map = db.getTreeMap("asta");
		
		ArrayList<Asta> listaAste=new ArrayList<Asta>();
		Set<Integer> keys = map.keySet();
		for(int key : keys) {
				if((map.get(key).getProprietario()).equals(input)){
					listaAste.add(map.get(key));
				}
			}
		return listaAste;
		
	
	}
	
	//Carica aste per utente
		@Override
		public ArrayList<Offerta> loadOffertaUtente(String input) {
			DB db = getDB();
			Map<Integer, Offerta> map = db.getTreeMap("offerta");
			ArrayList<Offerta> listaOfferte=new ArrayList<Offerta>();
			Set<Integer> keys = map.keySet();
			for(int key : keys) {
				if((map.get(key).getUtente()).equals(input)){
					listaOfferte.add(map.get(key));
				}
			}
			for(int i=0; i<listaOfferte.size();i++){
				Offerta temp = listaOfferte.get(i);
				for(int j=i+1; j<listaOfferte.size();j++){
					Offerta temp1 = listaOfferte.get(j);
					if(temp.getCodiceAsta()==temp1.getCodiceAsta()){
						if(temp.getImporto()>temp1.getImporto()) listaOfferte.remove(j);
						else listaOfferte.remove(i);
					}
				}
			}
			return listaOfferte;
		}
		
		//Carica descrizione Asta
		@Override
		public String descrAsta(int input){
			DB db = getDB();
			Map<Integer, Asta> map = db.getTreeMap("asta");
			Set<Integer> keys = map.keySet();
			for(int key : keys){
				if(map.get(key).getCodiceAsta()==input) return map.get(key).getDescrizione();
			}
			return "Errore";
		}
		
		@SuppressWarnings("deprecation")
		private int statusAsta(String dataAsta){
			Date today = new Date();
			int anno=Integer.parseInt(dataAsta.substring(0,4));
			int mese=Integer.parseInt(dataAsta.substring(5,7));
			int giorno=Integer.parseInt(dataAsta.substring(8,10));
			int giornoOggi = today.getDate();
			int meseOggi=today.getMonth()+1;
			int annoOggi=today.getYear()+1900;
			int stato=0;
		    if(anno>annoOggi||(anno==annoOggi&&mese>meseOggi)||(anno==annoOggi&&mese==meseOggi&&giorno>giornoOggi)) 
		    	stato=1;
		    return stato;
		}
		
		//Stato Offerta
		@Override
		public String statoOfferta(int input){
			DB db = getDB();
			Map<Integer, Offerta> map = db.getTreeMap("offerta");
			double impOff = map.get(input).getImporto();
			int codAsta = map.get(input).getCodiceAsta();
			int status=0;
			Set<Integer> keys = map.keySet();
			for(int key : keys){
				if(map.get(key).getCodiceAsta()==codAsta){
					if(map.get(key).getImporto()>impOff) status=1;
				}
			}
			DB db1 = getDB();
			Map<Integer, Asta> map1 = db1.getTreeMap("asta");
			int cntrl= statusAsta(map1.get(codAsta).getDataScadenza());
		    if(status==0)
		    	if(cntrl==1) return "Asta in corso e offerta migliore";
		    if(status==1)
		    	if(cntrl==1) return "Asta in corso e offerta superata";
		    if(status==1)
		    	if(cntrl==0) return "Asta conclusa e offerta superata";
		    return "Asta conclusa e offerta migliore";
		}
}

